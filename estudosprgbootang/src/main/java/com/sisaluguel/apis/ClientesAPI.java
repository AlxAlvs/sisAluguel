package com.sisaluguel.apis;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sisaluguel.dtos.ClienteDTO;
import com.sisaluguel.entidades.Cliente;
import com.sisaluguel.repositorios.ClientesRepository;

@RestController
@RequestMapping("/clientes")
public class ClientesAPI {
	
	@Autowired
	private ClientesRepository clientesRepository;
	
	@GetMapping
	public List<ClienteDTO> buscarTodos(@RequestParam(value = "nome", required = false) String nome) {
		if (nome == null || nome.equals("")) {
			return this.clientesRepository.buscarTodos();
		} else {
			return this.clientesRepository.buscarPorNome(nome);
		}
	}
	
	@PostMapping
	public void salvarCliente(@RequestBody Cliente cliente) {
		this.clientesRepository.save(cliente);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable("id") Integer id) {
		this.clientesRepository.deleteById(id);
	}
	
	@PutMapping("/{id}")
	public void alterarCliente(@PathVariable("id") Integer id, @RequestBody Cliente cliente) {
		cliente.setId(id);
		this.clientesRepository.save(cliente);
	}

}
