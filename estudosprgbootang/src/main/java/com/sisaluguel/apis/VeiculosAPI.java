package com.sisaluguel.apis;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sisaluguel.dtos.VeiculosConsultaDTO;
import com.sisaluguel.entidades.Veiculo;
import com.sisaluguel.repositorios.VeiculosRepository;

@RestController
@RequestMapping("/veiculos")
public class VeiculosAPI {

	@Autowired
	public VeiculosRepository veiculosRepository;
	
	@GetMapping
	public List<VeiculosConsultaDTO> getTodos(@RequestParam(value="descricao", required=false) String descricao) {
		if (descricao == null) {
			return this.veiculosRepository.findAllAsDTO();
		} else {
			return this.veiculosRepository.findAllByDescricaoAsDTO(descricao);
		}
	}
	
	@PostMapping
	public void salvarVeiculo(@RequestBody Veiculo veiculo) {
		this.veiculosRepository. save(veiculo);
	}
	
	@PutMapping("/{id}")
	public void alterarVeiculo(@PathVariable("id") Integer id, @RequestBody Veiculo veiculo) {
		veiculo.setId_veiculo(id);
		this.veiculosRepository.save(veiculo);
	}
	
	@DeleteMapping("/{id}")
	public void excluirVeiculo(@PathVariable("id") Integer id) {
		this.veiculosRepository.deleteById(id);
	}
}
