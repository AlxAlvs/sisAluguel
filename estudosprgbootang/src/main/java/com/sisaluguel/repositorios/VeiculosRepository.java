package com.sisaluguel.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sisaluguel.dtos.VeiculosConsultaDTO;
import com.sisaluguel.entidades.Veiculo;

public interface VeiculosRepository extends JpaRepository<Veiculo, Integer>{
	
	@Query("select new com.sisaluguel.dtos.VeiculosConsultaDTO " +
			   "(v.id, v.descricao, v.valor_veiculo, v.alugado) " +
			   "from Veiculo v " +
			   "order by v.descricao ")
	List<VeiculosConsultaDTO> findAllAsDTO();
	
	@Query("select new com.sisaluguel.dtos.VeiculosConsultaDTO " +
			   "(v.id, v.descricao, v.valor_veiculo, v.alugado) " +
			   "from Veiculo v " +
			   "where (v.descricao = :descricao) " +
			   "order by v.descricao ")
	List<VeiculosConsultaDTO> findAllByDescricaoAsDTO(@Param("descricao") String descricao);

}
