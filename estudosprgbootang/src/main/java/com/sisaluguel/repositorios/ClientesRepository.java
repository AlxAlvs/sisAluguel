package com.sisaluguel.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.sisaluguel.dtos.ClienteDTO;
import com.sisaluguel.entidades.Cliente;

public interface ClientesRepository extends CrudRepository<Cliente, Integer>{
	
	@Query("select new com.sisaluguel.dtos.ClienteDTO "
			+ "(c.id, c.nome. c.cpf) from Cliente c order by c.nome")
	List<ClienteDTO> buscarTodos();
	
	@Query("select new com.sisaluguel.dtos.ClienteDTO "
			+ "(c.id, c.nome, c.cpf) from Cliente c "
			+ "where c.nome = :nome order by c.nome")
	List<ClienteDTO> buscarPorNome(@Param("nome") String nome);

}
