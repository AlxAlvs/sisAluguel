import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'; 

import { VeiculosRoutingModule } from './veiculos-routing.module';
import { VeiculosListarComponent } from './veiculos-listar/veiculos-listar.component';

@NgModule({
  imports: [
    CommonModule,
    VeiculosRoutingModule,
    FormsModule
  ],
  declarations: [VeiculosListarComponent]
})
export class VeiculosModule { }
