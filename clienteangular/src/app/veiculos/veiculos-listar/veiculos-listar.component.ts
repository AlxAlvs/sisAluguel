import { Component, OnInit } from '@angular/core';
import Veiculo from '../../models/Veiculo';
import { VeiculosService } from '../../services/veiculos.service';

declare var bootbox: any;

@Component({
  selector: 'app-veiculos-listar',
  templateUrl: './veiculos-listar.component.html',
  styleUrls: ['./veiculos-listar.component.css']
})
export class VeiculosListarComponent implements OnInit {

  veiculos:Veiculo [] = [];

  descricao:string ;

  constructor(private veiculoService:VeiculosService) { }

  ngOnInit() {
    this.carregarVeiculos();
  }

  filtrar() {
    this.carregarVeiculos();
  }

  carregarVeiculos(){

    this.veiculoService
      .buscarPorFiltro(this.descricao)
      .subscribe(veiculos=>{
        this.veiculos = veiculos;
    })
  }
  
}
