import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VeiculosListarComponent } from './veiculos-listar/veiculos-listar.component';

const routes: Routes = [
  {path:'', component:VeiculosListarComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VeiculosRoutingModule { }
