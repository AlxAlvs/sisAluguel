export default interface Veiculo {
    idVeiculo:number;
    descricao:string;
    valor_veiculo:number;
    alugado:boolean;
}